# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::Coordinator do
  let(:versions) do
    [
      ReleaseTools::Version.new('15.5.5'),
      ReleaseTools::Version.new('15.4.5'),
      ReleaseTools::Version.new('15.3.6')
    ]
  end

  let(:commits) do
    instance_double(ReleaseTools::PatchRelease::UnreleasedCommits)
  end

  subject(:coordinator) { described_class.new }

  before do
    projects = [
      ReleaseTools::Project::GitlabEe,
      ReleaseTools::Project::OmnibusGitlab
    ]

    stub_const("ReleaseTools::ManagedVersioning::PROJECTS", projects)

    allow(ReleaseTools::Versions)
      .to receive(:next_versions)
      .and_return(versions)
  end

  describe '#pressure' do
    before do
      allow(ReleaseTools::PatchRelease::UnreleasedCommits)
        .to receive(:new)
        .and_return(commits)
    end

    context 'with unreleased commits' do
      it 'returns the total per version' do
        allow(commits)
          .to receive(:total_pressure)
          .and_return(3)

        pressure = {
          'gitlab-org/gitlab' => 3,
          'gitlab-org/omnibus-gitlab' => 3
        }

        output = [
          { version: '15.5', pressure: pressure },
          { version: '15.4', pressure: pressure },
          { version: '15.3', pressure: pressure }
        ]

        expect(coordinator.pressure).to eq(output)
      end
    end

    context 'without unreleased commits' do
      it 'returns 0 per version' do
        allow(commits)
          .to receive(:total_pressure)
          .and_return(0)

        pressure = {
          'gitlab-org/gitlab' => 0,
          'gitlab-org/omnibus-gitlab' => 0
        }

        output = [
          { version: '15.5', pressure: pressure },
          { version: '15.4', pressure: pressure },
          { version: '15.3', pressure: pressure }
        ]

        expect(coordinator.pressure).to eq(output)
      end
    end
  end

  describe '#merge_requests' do
    let(:output) do
      [
        { 'id' => 1, 'title' => 'foo', 'web_url' => 'https://foo.com' },
        { 'id' => 2, 'title' => 'bar', 'web_url' => 'https://bar.com' }
      ]
    end

    let(:gitlab_merge_requests) do
      instance_double(ReleaseTools::PatchRelease::UnreleasedMergeRequests, execute: output)
    end

    let(:omnibus_merge_requests) do
      instance_double(ReleaseTools::PatchRelease::UnreleasedMergeRequests, execute: [])
    end

    before do
      allow(ReleaseTools::PatchRelease::UnreleasedMergeRequests)
        .to receive(:new).with(ReleaseTools::Project::GitlabEe, any_args)
        .and_return(gitlab_merge_requests)

      allow(ReleaseTools::PatchRelease::UnreleasedMergeRequests)
        .to receive(:new).with(ReleaseTools::Project::OmnibusGitlab, any_args)
        .and_return(omnibus_merge_requests)
    end

    it 'returns the merge_requests per version by project' do
      merge_requests = {
        'gitlab-org/gitlab' => output,
        'gitlab-org/omnibus-gitlab' => []
      }

      output = [
        { version: '15.5', pressure: 2, merge_requests: merge_requests },
        { version: '15.4', pressure: 2, merge_requests: merge_requests },
        { version: '15.3', pressure: 2, merge_requests: merge_requests }
      ]

      expect(coordinator.merge_requests).to eq(output)
    end
  end
end
