# frozen_string_literal: true

module ReleaseTools
  module ManagedVersioning
    # List of projects under the Managed Versioning model.
    # Stable branches and tags for these projects are automatically
    # created.
    PROJECTS = [
      ReleaseTools::Project::GitlabEe,
      ReleaseTools::Project::OmnibusGitlab,
      ReleaseTools::Project::Gitaly,
      ReleaseTools::Project::CNGImage
    ].freeze
  end
end
