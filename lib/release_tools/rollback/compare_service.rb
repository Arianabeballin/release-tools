# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Generates a `Rollback::Comparison` between a current state and a target to
    # which we want to roll back.
    #
    # The `current` and the `target` values should be a package String (e.g.,
    # `14.3.202109151120-97529725116.5a421e7fb6d`).
    #
    # The `environment` argument should be one of `gstg-cny`, `gstg`, `gprd-cny`
    # or `gprd`.
    #
    # We fetch the Deployments for the given environment. When the `current` argument
    # is nil, we grab the latest Deployment regardless of status; when the `target`
    # argument is nil, we grab the latest successful Deployment that isn't the
    # current one.
    #
    # Examples:
    #
    #   # Comparing two specific packages
    #   CompareService.new(
    #     current: '14.3.202109151620-603b96d4843.7b4f35cc217',
    #     target: '14.3.202109151120-97529725116.5a421e7fb6d',
    #     environment: 'gstg'
    #   ).execute
    #
    #   # Comparing current and previous `gprd` deployments
    #   CompareService.new(environment: 'gprd').execute
    #
    #   # Comparing current `gprd` deployment to a specific version
    #   CompareService
    #     .new(environment: 'gprd', target: '14.3.202109151120-97529725116.5a421e7fb6d')
    #     .execute
    class CompareService
      include ::SemanticLogger::Loggable

      PROJECT = Project::Release::Metadata

      ENVIRONMENTS = %w[gstg-cny gstg gprd-cny gprd].freeze

      attr_reader :current, :target, :environment

      def initialize(environment:, current: nil, target: nil)
        @current = from_package(current)
        @target = from_package(target)
        @environment = environment
      end

      def execute
        validate_environment!

        @current = current_from_deployment if @current.nil?
        @target = target_from_deployment if @target.nil? || environment?(@target)

        logger.info("Generating comparison", current: @current.to_s, target: @target.to_s)

        validate_version!(@current)
        validate_version!(@target)

        @comparison = Rollback::Comparison
          .new(current: @current, target: @target, environment: environment)
          .execute
      end

      private

      def validate_version!(version)
        return if version.is_a?(ProductVersion)

        logger.fatal("Invalid version for comparison", version: version.to_s)
        raise ArgumentError
      end

      def environment?(name)
        ENVIRONMENTS.include?(name.to_s)
      end

      def validate_environment!
        return if environment?(environment)

        logger.fatal("Invalid environment for comparison", environment: environment)
        raise ArgumentError
      end

      def from_package(package)
        return package if package.nil?

        # If it doesn't match it may be an environment, which we'll load later
        return package unless AutoDeploy::Version.match?(package)

        ProductVersion.from_auto_deploy(package)
      end

      def from_metadata_commit_id(sha)
        return nil if sha.nil?

        ProductVersion.from_metadata_sha(sha)
      end

      def current_from_deployment
        from_metadata_commit_id(deployments.first.sha)
      end

      def target_from_deployment
        target = deployments.detect do |dep|
          dep.sha != @current.metadata_commit_id && dep.status == 'success'
        end

        from_metadata_commit_id(target&.sha)
      end

      def deployments
        Retriable.with_context(:api) do
          GitlabOpsClient.deployments(PROJECT, environment)
        end
      end
    end
  end
end
