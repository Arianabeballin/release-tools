# frozen_string_literal: true

module ReleaseTools
  module Promotion
    module Checks
      # ChangeRequests checks for in-progress change requests issue with C1 or C2 label
      class ChangeRequests
        include ProductionIssueTracker

        # @param scope [Symbol] can be either :deployment or :feature_flag.
        def initialize(scope:)
          @scope = scope
        end

        def name
          'change requests in progress'
        end

        def labels
          'change::in-progress'
        end

        private

        attr_reader :scope
      end
    end
  end
end
