# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ProductionCheck
      # Perform a production health check via ChatOps
      #
      # Results are posted to a specified channel.
      class Chatops
        include ::SemanticLogger::Loggable

        UnsafeProductionError = Class.new(StandardError)
        UnknownScopeError = Class.new(StandardError)

        def initialize
          @channel = ENV.fetch('CHAT_CHANNEL')
        end

        def execute
          notify_on_slack(status.to_slack_blocks)

          return if status.fine?

          raise UnsafeProductionError if raise_on_failure?
        end

        def notify_on_slack(blocks)
          return if SharedStatus.dry_run?

          Retriable.retriable do
            Slack::ChatopsNotification.fire_hook(channel: @channel, blocks: blocks)
          end
        rescue ReleaseTools::Slack::Webhook::CouldNotPostError
          logger.error('Malformed Slack request', channel: @channel, blocks: blocks.to_json)

          raise
        end

        private

        def raise_on_failure?
          ENV['FAIL_IF_NOT_SAFE'] == 'true'
        end

        def scope
          logger.info('PRODUCTION_CHECK_SCOPE value', production_check_scope: ENV.fetch('PRODUCTION_CHECK_SCOPE'))
          value = ENV.fetch('PRODUCTION_CHECK_SCOPE', 'deployment')

          raise UnknownScopeError, "'#{value}' is not a valid scope" unless %w(deployment feature_flag).include?(value)

          value.to_sym
        end

        def status
          @status ||= Promotion::ProductionStatus.new(*checks, scope: scope)
        end

        def checks
          if ENV.key?('SKIP_DEPLOYMENT_CHECK')
            %i[canary_up]
          else
            %i[canary_up active_gprd_deployments]
          end
        end
      end
    end
  end
end
