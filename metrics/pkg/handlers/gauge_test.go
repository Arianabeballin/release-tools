package handlers

import (
	"net/http"
	"testing"
)

type mockGauge struct {
	mockCounter
}

func (m *mockGauge) Set(value float64, labels ...string) {
	m.cnt = value
}

func (m *mockGauge) Dec(labels ...string) {
	m.cnt--
}

func TestGaugeIncHandler(t *testing.T) {
	meta := &mockGauge{}
	meta.expectedLabels = []string{"red"}
	handler := NewGauge(meta).(*gauge)

	req, err := meta.apiRequest("inc", "")
	if err != nil {
		t.Fatal(err)
	}

	rr := testRequest(req, handler)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if meta.cnt != 1 {
		t.Errorf("Metrics not incremented: expected 1 got %v", meta.cnt)
	}
}

func TestGaugeSetHandler(t *testing.T) {
	meta := &mockGauge{}
	meta.expectedLabels = []string{"red", "high"}
	handler := NewGauge(meta).(*gauge)

	req, err := meta.apiRequest("set", "1.5")
	if err != nil {
		t.Fatal(err)
	}

	rr := testRequest(req, handler)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if meta.cnt != 1.5 {
		t.Errorf("Metrics not incremented: expected 1.5 got %v", meta.cnt)
	}
}
