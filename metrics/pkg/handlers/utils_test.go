package handlers

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
)

func apiRequest(meta metrics.Metadata, action, value, labels string) (*http.Request, error) {
	formData := url.Values{
		"value":  {value},
		"labels": {labels},
	}

	req, err := http.NewRequest("POST", route(meta)+"/"+action, strings.NewReader(formData.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	return req, nil
}

func testRequest(req *http.Request, h Pluggable) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	handler := mux.NewRouter()

	h.PlugRoutes(handler)
	handler.ServeHTTP(rr, req)

	return rr
}

type mockMetric struct {
	//expectedLabels stores the expected values from the incoming request,
	// each element is the label value we expect to receive during the test
	expectedLabels []string
}

func (m *mockMetric) Namespace() string {
	return "test"
}

func (m *mockMetric) Name() string {
	return "name"
}

func (m *mockMetric) Subsystem() string {
	return "subsystem"
}

func (m *mockMetric) apiRequest(action, value string) (*http.Request, error) {
	return apiRequest(m, action, value, strings.Join(m.expectedLabels, ","))
}

func (m *mockMetric) CheckLabels(labels []string) error {
	if len(labels) != len(m.expectedLabels) {
		return fmt.Errorf("expected %v labels, got %v", len(m.expectedLabels), len(labels))
	}

	for i, label := range labels {
		if label != m.expectedLabels[i] {
			return fmt.Errorf("Label %v - expected %q got %q", i, m.expectedLabels[i], label)
		}
	}

	return nil
}
