package labels

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestStringSliceName(t *testing.T) {
	name := "a_label"

	l := FromValues(name, []string{"foo", "bar"})

	require.Equal(t, name, l.Name(), "label name must match the constructor parameter")
}

func TestStringSliceValues(t *testing.T) {
	colours := []string{"red", "green"}
	label := FromValues("color", colours)

	require.Equal(t, colours, label.Values(), "values must matche the constructor parameter")
}

func TestStringSliceCheckValue(t *testing.T) {
	colours := []string{"red", "green"}
	label := FromValues("color", colours)

	for _, color := range colours {
		assert.Truef(t, label.CheckValue(color), "%q must be a valid label value", color)
	}

	assert.False(t, label.CheckValue("foo"), "\"foo\" is not a valid \"color\" value")
}
