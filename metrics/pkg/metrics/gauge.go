package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Gauge interface {
	Metadata
	Inc(labels ...string)
	Dec(labels ...string)
	Set(value float64, labels ...string)
}

type gaugeVec struct {
	*description

	metric *prometheus.GaugeVec
}

func NewGaugeVec(opts ...MetricOption) (Gauge, error) {
	descOpts := applyMetricOptions(opts)

	prom := promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: descOpts.Namespace(),
			Subsystem: descOpts.Subsystem(),
			Name:      descOpts.Name(),
			Help:      descOpts.help,
		}, descOpts.labelsNames())

	for _, labels := range descOpts.labelsToInitialize {
		_, e := prom.GetMetricWithLabelValues(labels...)
		if e != nil {
			return nil, e
		}
	}

	return &gaugeVec{
		description: descOpts.description,
		metric:      prom,
	}, nil
}

func (g *gaugeVec) Inc(labels ...string) {
	g.metric.WithLabelValues(labels...).Inc()
}

func (g *gaugeVec) Dec(labels ...string) {
	g.metric.WithLabelValues(labels...).Dec()
}

func (g *gaugeVec) Set(value float64, labels ...string) {
	g.metric.WithLabelValues(labels...).Set(value)
}
